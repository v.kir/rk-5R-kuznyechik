CC=g++
CFLAGS=-c -std=c++11 -O2 -Wall
LDFLAGS=-static
SOURCES=main.cpp lsx.cpp generation.cpp experiment.cpp difference.cpp common.cpp
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=attack

all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm *.o attack
